package com.dztech.guitaristapp.repository;

import com.dztech.guitaristapp.model.Guitarist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GuitaristRepository extends JpaRepository<Guitarist, Long> {
}
