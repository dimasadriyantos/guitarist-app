package com.dztech.guitaristapp.controller;

import com.dztech.guitaristapp.exception.ResourceNotFoundException;
import com.dztech.guitaristapp.model.Guitarist;
import com.dztech.guitaristapp.service.GuitaristService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping("api/v1/guitaristApp")
public class GuitaristController {


    @Autowired
    GuitaristService guitaristService;

    @PostMapping
    public ResponseEntity<Guitarist> createAccount(@RequestBody Guitarist guitarist) {
        try {
            Guitarist result = guitaristService.createAccount(guitarist);
            return new ResponseEntity(result, HttpStatus.CREATED);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

   /* @GetMapping(path = "{id}")
    public ResponseEntity<Guitarist> getAccountById(@PathVariable("id") long id) {
        Guitarist result = guitaristRepository
                .findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Not found Account with Id = " + id));

        return new ResponseEntity<>(result, HttpStatus.OK);
        *//*Optional<Guitarist> result = guitaristRepository.findById(id);

        if (result.isPresent()) {
            return new ResponseEntity(result, HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Not found Account with Id = " + id +" Please insert correct Id");
        }*//*
    }

    @GetMapping
    public ResponseEntity<Guitarist> getAllAccount() {
        List<Guitarist> result = new ArrayList<>();
        guitaristRepository.findAll().forEach(result::add);
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @PutMapping("{id}")
    public ResponseEntity<Guitarist> updateAccount(@PathVariable("id") long id, @RequestBody Guitarist guitarist) {

        Optional<Guitarist> data = guitaristRepository.findById(id);

        if (data.isPresent()) {
            Guitarist newGuitarist = data.get();
            newGuitarist.setName(guitarist.getName());
            newGuitarist.setBio(guitarist.getBio());
            newGuitarist.setGenre(guitarist.getGenre());
            newGuitarist.setRating(guitarist.getRating());
            return new ResponseEntity<>(guitaristRepository.save(newGuitarist), HttpStatus.OK);
        } else {
            throw new ResourceNotFoundException("Not found Account with Id = " + id + " Please insert correct Id");
        }
    }*/


}
