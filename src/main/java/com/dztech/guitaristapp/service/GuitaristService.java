package com.dztech.guitaristapp.service;

import com.dztech.guitaristapp.model.Guitarist;


public interface GuitaristService {

    Guitarist createAccount(Guitarist guitarist);

}
