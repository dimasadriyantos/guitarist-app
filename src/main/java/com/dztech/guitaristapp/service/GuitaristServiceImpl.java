package com.dztech.guitaristapp.service;

import com.dztech.guitaristapp.model.Guitarist;
import com.dztech.guitaristapp.repository.GuitaristRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GuitaristServiceImpl implements GuitaristService {

    @Autowired
    GuitaristRepository guitaristRepository;

    public Guitarist createAccount(Guitarist guitarist) {
        Guitarist result = new Guitarist();

        result.setName(guitarist.getName());
        result.setBio(guitarist.getBio());
        result.setRating(guitarist.getRating());
        result.setGenre(guitarist.getGenre());

        return guitaristRepository.save(result);
    }


}
