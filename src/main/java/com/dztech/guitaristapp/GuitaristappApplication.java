package com.dztech.guitaristapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuitaristappApplication {

    public static void main(String[] args) {
        SpringApplication.run(GuitaristappApplication.class, args);
    }

}
