package com.dztech.guitaristapp.exception;

public class ResourceNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 9023200691331907589L;

    public ResourceNotFoundException(String message) {
        super(message);
    }


}
